## Управление отношениями с руководителем

## Письмо руководителю о предоставлении обратной связи

День добрый, Александр Иванович.  

Прошу вас предоставить мне обратную связь за отработанную первую неделю моей стажировки.  
Хочу услышать объективное впечатление обо мне как потенциальном сотруднике компании.   
Что мне надо подтянуть, что исправить.  
Прошу назначить мне наставника на практические задания.  

С уважением Мурад.  
Стажер-Тестировщик ПО


## Что можно узнать о себе на сессии обратной связи по проекту?

1. Корректно ли и технически правильно ли я выполнил порученное задание.
2. Уложился ли я в сроки выполнения задания, можно было ли его выполнить быстрее.
3. Внимательно ли я выполнил задание, не допустил ли ошибок при выполение задания.
4. Как я и как со мной взаимодействовал коллектив. Не мешал ли я им, смог ли я оказать помощь команде в выполнение проекта.
5. Что мне необходимо дополнительно изучить чтобы быть более полезным команде.